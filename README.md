# Sistem za vodenje konference 

## Namen projekta
Cilj naše aplikacije je pomagati organizirati konferenco na lažji način, preko enotnega vmesnika. Cilj aplikacije je tudi izboljšati samo preglednost oseb ki se nameravajo udeležiti konference, ter udeležencev.

## Funkcionalnosti
-	**Ustvarjanje konference**
-	**Vnos prostora konference**
-	**Pošiljanje vabil za udeležbo**
-	**Pošiljanje obvestil prijavljenim uporabnikom**
-	**Vnos gradiva**
-	**Ustvarjanje urnika**
-	**Seznam prijavljenih uporabnikov**
-	**Seznam udeležencev konference**
-	**Ustvarjanje QR-kode**
-	**Pregled podatkov konference**
-	**Branje QR-kode**
-	**Prenos objavljenega gradiva**

## Razvojno okolje
- VisualStudio Code

## GitLab repozitorij
Celoten repozitorij celotnega projekta se nahaja na [GitLab-u](https://gitlab.com/PojbicGregor/sistem-za-vodenje-strokovne-konference).

## Vzpostavitev projekta
Ker se projekt nahaja na GitLab-u, lahko vzpostavimo projekt tako da kloniramo [GitLab repozitorij](https://gitlab.com/PojbicGregor/sistem-za-vodenje-strokovne-konference).

## Zagon projekta
### Zagon backenda
Backend zaženemo v mapi ```\backend>``` z ukazom ```nodemon index.js``` ali samo ```nodemon``` lahko pa tudi z ukazom ```node index.js```.

### Zagon frontenda
Frontend zaženemo v mapi ```\frontend>``` z ukazom ```npm start```.

## Namestitev projekta
Projekt se lahko namesti na hosting ponudnika preko repozitorija. Opis tega se nahaja v [dokumentaciji](Documentation/Sistem_za_vodenje_konference.docx).

## Vloge
Uporabnik/udeleženec konference
username: 123@yahoo.com
password: 123456

Organizator
username: jan@yahoo.com
password: 123456

## Člani
- **Pojbič Gregor**
- **Manev Blazhe**
- **Lukač Jan**
